
#[derive(Hash)]
pub struct Secret {
    id : String, //identifies a secret
    attribute : String, //points to the attribute of the secret, e.g. username or password
}
impl Secret {
    pub fn new(id: String, attribute: String)-> Secret {
        Secret {id: id, attribute: attribute}
    }
}

impl PartialEq for Secret{
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id && self.attribute == other.attribute
    }
}
impl Eq for Secret{}

impl ToString for Secret {
    // Required method
    fn to_string(&self) -> String {
        format!("{{{{{}::{}}}}}",self.id,self.attribute)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_secret_get_string() {
        let secret = Secret::new(String::from("test"),String::from("something"));
        let sec_str = secret.to_string();
        assert!(sec_str == String::from("{{test::something}}"),"we got : {}", sec_str);            
    }
}
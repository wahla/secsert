use crate::secret::Secret;

pub trait SecretProvider {
    fn resolve(&self, secret: &Secret) -> &String;
}
use std::collections::HashMap;
use crate::secret::Secret;
use super::secretprovider::SecretProvider;

pub struct SimpleProvider {
    pub keyvalue: HashMap<Secret, String>,
}

impl SecretProvider for SimpleProvider {
    fn resolve(&self, secret: &Secret) -> &String {
        self.keyvalue.get(secret).unwrap()
    }
}

impl SimpleProvider {
    pub fn new() -> SimpleProvider {
        SimpleProvider {
            keyvalue: HashMap::new(),
        }
    }

    pub fn insert(&mut self, secret:Secret, value:String) {
        self.keyvalue.insert(secret,value);
    }
}
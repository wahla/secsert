
pub trait LineOutputSink {
    fn write(&mut self, line: &str);
}


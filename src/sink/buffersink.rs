use super::lineoutputsink::LineOutputSink;


pub struct BufferSink {
    pub result: String,
}

impl LineOutputSink for BufferSink {
    fn write(&mut self, line: &str) {
        self.result.push_str(line);
    }
}

impl BufferSink {
    pub fn new() -> Self {
        BufferSink {
            result : String::new(),
        }
    }
}
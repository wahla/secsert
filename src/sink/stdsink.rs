
use super::lineoutputsink::LineOutputSink;

pub struct StdSink {}
impl LineOutputSink for StdSink {
    fn write(&mut self, line: &str) {
        println!("{}",line);
    }
}
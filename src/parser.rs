use crate::secret::Secret;
use crate::sink::lineoutputsink::LineOutputSink;
use crate::provider::secretprovider::SecretProvider;
use regex::Regex;
use std::fs::File;
use std::io::BufReader;
use std::io::BufRead;


pub struct ParserJob<'a, T: SecretProvider, O: LineOutputSink> {
    filename: String,
    provider: T,
    output: &'a mut  O,
}

impl<'a, T, O> ParserJob<'a, T, O>
where
    T: SecretProvider,
    O: LineOutputSink,
{
    pub fn run(&mut self) {
        let filename = self.filename.as_str();
        debug!("input template : {filename}");
        //open the template and parse it line by line
        let file_handle = File::open(filename).unwrap();
        let mut reader = BufReader::new(file_handle);
        loop {
            let mut buffer_str = String::new();
            match reader.read_line(&mut buffer_str) {
                Ok(0) => break,
                Ok(_) => {
                    let processed=self.process_line(buffer_str.clone());
                    self.output.write(&processed);},
                Err(_) => panic!("error"),
            }
        }
    }

    fn process_line(&self, line: String) -> String {
        let secrets_in_line = get_templates_from_string(&line);
        let mut res = line;
        for secret_in_line in secrets_in_line {
            let secret_value = self.provider.resolve(&secret_in_line);
            res = res.replace(secret_in_line.to_string().as_str(), secret_value.as_str());
        }
        return res;
    }
}

fn get_templates_from_string(line: &String) -> Vec<Secret> {
    let input = line.as_str();
    let mut result: Vec<Secret> = Vec::new();
    let re = Regex::new(r"\{\{([a-zA-Z0-9]+)::([a-zA-Z0-9]+)\}\}").unwrap();
    //let captures = re.captures(&input).unwrap();
    for (_, [id, attribute]) in re.captures_iter(input).map(|c| c.extract()) {
        let secret = Secret::new(String::from(id), String::from(attribute));
        result.push(secret);
    }
    return result;
}

pub fn new<'a,T: SecretProvider, O: LineOutputSink>(
    filename: &str,
    provider: T,
    output: & 'a mut  O,
) -> ParserJob<'a,T, O> {
    ParserJob {
        filename: String::from(filename),
        provider,
        output,
    }
}

#[cfg(test)]
mod tests {
    use std::io::BufRead;
    use super::*;
    use crate::sink::buffersink::BufferSink;
    use crate::provider::simple::SimpleProvider;

    #[test]
    fn process_run() {
        let my_secret = Secret::new(String::from("mykey"), String::from("attr"));
        let mut provider = SimpleProvider::new();
        provider
            .keyvalue
            .insert(my_secret, String::from("that makes no sense"));

        provider.insert(Secret::new(String::from("sec"), String::from("no1")), "Lorem".to_owned());
        provider.insert(Secret::new(String::from("abc"), String::from("pwd")), "sed".to_owned());
        provider.insert(Secret::new(String::from("xyzabc"), String::from("foo")), "amet".to_owned());
        let mut sink = BufferSink::new();
        let mut job = new("./src/testdata/test.txt", provider, &mut sink);
        job.run();

        //now open "should" data file and compare
        let file_handle = File::open("./src/testdata/test.orig.txt").unwrap();
        let mut reader = BufReader::new(file_handle);

        let mut should = String::new();
        loop {
            match reader.read_line(&mut should) {
                Ok(0) => {
                    //reached testfile EOF
                    break;
                }
                Ok(1) => {
                    println!("empty line in testfile");
                }
                Ok(_) => {}
                Err(_) => panic!("error reading test data")
            }
        }
        assert_eq!(should,sink.result);
    }

    #[test]
    fn test_get_templates_from_string() {
        let result = get_templates_from_string(&String::from(
            "this is a placeholder {{key1::USER}} and another {{otherid::passWORD}} blah",
        ));
        let secret1 = Secret::new(String::from("key1"), String::from("USER"));
        let secret2 = Secret::new(String::from("otherid"), String::from("passWORD"));

        assert!(result.len() == 2);
        assert!(result[0] == secret1);
        assert!(result[1] == secret2);
    }

    #[test]
    fn test_process_line_with_simpleprovider() {
        let my_secret = Secret::new(String::from("mykey"), String::from("attr"));
        let mut provider = SimpleProvider::new();
        provider.insert(my_secret, String::from("that makes no sense"));
        let mut sink = BufferSink::new();
        let job = new("filename", provider, &mut sink);
        let res = job.process_line(String::from("line with data {{mykey::attr}}"));
        let should = String::from("line with data that makes no sense");
        assert!(res == should, "expectation:  {} reality: {}", should, res);
    }
}

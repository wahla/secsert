const VERSION: &'static str = env!("CARGO_PKG_VERSION");
const AUTHOR: &'static str = env!("CARGO_PKG_AUTHORS");

mod parser;
mod provider {
    pub mod secretprovider;
    pub mod simple;
}
mod secret;
mod sink {
    pub mod lineoutputsink;
    pub mod stdsink;
    #[cfg(test)]
    pub mod buffersink;
}

use clap::{Arg, Command};
extern crate pretty_env_logger;
#[macro_use]
extern crate log;
use log::LevelFilter;
use crate::sink::stdsink::StdSink;

use crate::provider::simple::SimpleProvider;

#[tokio::main]
async fn main() {
    pretty_env_logger::formatted_builder()
        .filter_level(LevelFilter::Debug)
        .init();
    let args = Command::new("secsert")
        .version(VERSION)
        .author(AUTHOR)
        .about("Secret Inserter")
        .arg(
            Arg::new("input")
                .required(true)
                .help("the name of the template file"),
        )
        .get_matches();

    if let Some(filename) = args.get_one::<String>("input") {
        let mut sink = StdSink{};
        let mut  pjob = parser::new(filename, SimpleProvider::new(),&mut sink);
        pjob.run();
    }
}
